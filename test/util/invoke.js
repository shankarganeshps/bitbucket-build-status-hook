// utility for shelling out to native stuff

var byline = require("byline");
var RSVP = require("rsvp");
var spawn = require("child_process").spawn;

module.exports = function(executable) {
    return function(args, opts) {
        return new RSVP.Promise(function(resolve, reject) {
            opts.logger('spawning ' + executable + ' ' + args.join(' ') + ' in ' + opts.cwd);
            var proc = spawn(executable, args, {
                stdio: (opts.stderr) ? ['pipe', 'pipe', process.stderr] : undefined,
                env: Object.assign({}, opts.env, process.env),
                cwd: opts.cwd
            });
            var procStream = byline(proc.stdout);
            procStream.on('data', function(buff) {
                opts.onLine && opts.onLine(buff.toString());
            });
            proc.on('close', function() {
                opts.onClose && opts.onClose();
            });
            proc.on('exit', function (code) {
                opts.logger(executable + ' ' + args.join(' ') + ' exited with ' + code);
                if (code) {
                    reject(executable + ' ' + args.join(' ') + ' exited with ' + code);
                } else {
                    resolve();
                }
            });
        });
    }
};

