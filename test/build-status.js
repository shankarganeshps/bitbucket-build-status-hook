var rewire = require("rewire");
var assert = require("chai").assert;
var sinon = require("sinon");
var RSVP = require("rsvp");

function falsePromise(val) {
    return function() {
        return new RSVP.Promise(function(resolve) {
            resolve(val);
        });
    }
}

var fakeProcess = {
    exit: function() {}
};

var fakeConsole = {
    log: function(val) {},
    error: function() {}
};

describe("build-status.js reading collated build data and reporting to stdout", function() {

    var buildStatus = rewire("../lib/build-status");
    buildStatus.__set__("parseCommitContext", falsePromise({
        shortSha: "deadb33f",
        commitURl: "http://example.com/commits/deadb33f"
    }));
    buildStatus.__set__("process", fakeProcess);
    var logSpy = sinon.spy(fakeConsole, "log");
    buildStatus.__set__("logger", fakeConsole);

    afterEach(function() {
        logSpy.reset();
    });

    describe("No builds", function() {

        it("prints has not built if no builds were found", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": [],
                "inprogress": [],
                "failed": []
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "hasn't been built");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

    });

    describe("Broken builds", function() {

        it("prints failed build if one failed build", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": [],
                "inprogress": [],
                "failed": ["Bad-Build-A"]
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "1 failed build");
                assert.include(message, "Bad-Build-A");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("prints failed build if two failed builds", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": [],
                "inprogress": [],
                "failed": ["Bad-Build-A", "Bad-Build-B"]
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "2 failed builds");
                assert.include(message, "Bad-Build-A");
                assert.include(message, "Bad-Build-B");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("prints failed build if one successful and two failed builds", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": ["Good-Build-A"],
                "inprogress": [],
                "failed": ["Bad-Build-A", "Bad-Build-B"]
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "2 failed builds");
                assert.include(message, "Bad-Build-A");
                assert.include(message, "Bad-Build-B");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("prints failed build if one successful, one in progress and two failed builds", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": ["Good-Build-A"],
                "inprogress": ["In-Progress-Build-A"],
                "failed": ["Bad-Build-A", "Bad-Build-B"]
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "2 failed builds");
                assert.include(message, "Bad-Build-A");
                assert.include(message, "Bad-Build-B");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

    });

    describe("In progress builds", function() {

        it("prints in progress build if one in progress build", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": [],
                "inprogress": ["In-Progress-A"],
                "failed": []
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "1 currently executing build");
                assert.include(message, "In-Progress-A");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("prints in progress builds if two in progress builds", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": [],
                "inprogress": ["In-Progress-A", "In-Progress-B"],
                "failed": []
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "2 currently executing builds");
                assert.include(message, "In-Progress-A");
                assert.include(message, "In-Progress-B");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("prints in progress builds if one successful, and two in progress builds", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": ["Good-Build-A"],
                "inprogress": ["In-Progress-A", "In-Progress-B"],
                "failed": []
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "2 currently executing builds");
                assert.include(message, "In-Progress-A");
                assert.include(message, "In-Progress-B");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

    });

    describe("Succssful builds", function() {

        it("prints successful builds if one successful build", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": ["Good-Build-A"],
                "inprogress": [],
                "failed": []
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "1 green build");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("prints successful builds if two successful builds", function(done) {
            buildStatus.__set__("fetchBuilds", falsePromise({
                "successful": ["Good-Build-A", "Good-Build-B"],
                "inprogress": [],
                "failed": []
            }));

            buildStatus({}).then(function() {
                var message = logSpy.getCall(0).args[0];
                assert.include(message, "deadb33f");
                assert.include(message, "2 green builds");
                done();
            }).catch(function (err) {
                done(err);
            });
        });

    });
    
});