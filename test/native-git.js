var git = require("./util/git");
var bbs = require("./util/bbs");
var temp = require("temp").track();
var fs = require("fs");
var path = require("path");
var jsonfile = require("jsonfile");
var assert = require("chai").assert;

var packageJson = jsonfile.readFileSync(path.join(__dirname, '../package.json'));
var repoBitbucketUrl = packageJson.repository.url;

function prettyLog(msg) {
    console.error("\t" + msg);
}

describe("Integration tests with native git and Bitbucket", function() {

    var localClone;
    var testRepo;
    var hookOutput;

    before(function(done) {
        this.timeout(30000); // cloning over network might take a while
        localClone = temp.mkdirSync("bbsh-repo-");
        git(['clone', repoBitbucketUrl, localClone], {
            logger: prettyLog,
            onLine: prettyLog
        })
        .then(done)
        .catch(function (err) {
            fail('git clone should exit without error', err);
        });
    });
    
    beforeEach(function(done) {
        this.timeout(5000); // local clone should be much quicker
        testRepo = temp.mkdirSync("bbsh-repo-test-");
        hookOutput = temp.openSync("bbsh-hook-");
        prettyLog("Dumping hook output to " + hookOutput.path);
        git(['clone', localClone, testRepo, "--quiet"], {
            stderr: true,
            logger: prettyLog,
            onLine: prettyLog
        })
        .then(function() {
            debugger;
            return git(['remote', 'set-url', 'origin', repoBitbucketUrl], {
                stderr: true,
                logger: prettyLog,
                onLine: prettyLog,
                cwd: testRepo
            });
        })
        .then(function() {
            // symlink post-checkout hook
            fs.symlinkSync(path.resolve(__dirname + "/../index.js"), testRepo + "/.git/hooks/post-checkout");
        })
        .then(done)
        .catch(function(err) {
            fail('git clone && git remote should exit without error', err);
        });
    });

    after(function() {
        temp.cleanupSync();
    });

    // utility function for checking out a particular commit, triggering the post-checkout hook, and asserting output
    function checkoutAndAssertHookOutputContainsStrings(sha, strings, done) {
        return git(["checkout", sha, "--quiet"], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo,
            env: {
                BB_SKIP_AUTH: 1,
                BB_HOOK_OUT: hookOutput.path
            }
        })
        .then(function() {
            var message = fs.readFileSync(hookOutput.path).toString();
            prettyLog("Hook output: " + message);
            strings.forEach(function(str) {
                assert.include(message, str);
            });
            done();
        })
        .catch(function(err) {
            done(err);
        });
    }

    function checkoutHeadExpectingError(strings, done) {
        return git(["checkout", "HEAD", "--quiet"], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo,
            env: {
                BB_SKIP_AUTH: 1,
                BB_HOOK_OUT: hookOutput.path
            }
        })
        .then(function() {
            done(new Error("Expected error, but checkout completed with normal status code"));
        })
        .catch(function(err) {
            // we expect git checkout to die with an error, but have to try/catch here in case an assertion (or
            // anything else) fails so we can pass the error to done()
            try {
                var message = fs.readFileSync(hookOutput.path).toString();
                prettyLog("Hook output: " + message);
                strings.forEach(function (str) {
                    assert.include(message, str);
                });
                // assertions all passed
                done();
            } catch (e) {
                // assertion or (something else) failed
                done(e);
            }
        });
    }

    function invokeBitbucketBuildStatusExpectingError(strings, done) {
        return bbs([], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo,
            env: {
                BB_SKIP_AUTH: 1,
                BB_HOOK_OUT: hookOutput.path
            }
        }).then(function() {
            done(new Error("Expected error, but process completed with normal status code"));
        }).catch(function(err) {
            // we expect bitbucket-build-status to die with an error, but have to try/catch here in case an assertion (
            // or anything else) fails so we can pass the error to done()
            try {
                var message = fs.readFileSync(hookOutput.path).toString();
                prettyLog("Hook output: " + message);
                strings.forEach(function (str) {
                    assert.include(message, str);
                });
                // assertions all passed
                done();
            } catch (e) {
                // assertion or (something else) failed
                done(e);
            }
        });
    }

    it("Not built yet", function(done) {
        this.timeout(10000); // allow time to query status API over network
        checkoutAndAssertHookOutputContainsStrings(
            "342e8fd73ab1f1f519b8af9bcaf71efa12c3bad8",
            ["342e8fd", "hasn't been built"],
            done
        );
    });

    it("1 successful build", function(done) {
        this.timeout(10000); // allow time to query status API over network
        checkoutAndAssertHookOutputContainsStrings(
            "c1ce4103d98c6ae06c0f3bd8fa135c57c609e6f1",
            ["c1ce410", "1 green build"],
            done
        );
    });

    it("1 successful, 1 failed build", function(done) {
        this.timeout(10000); // allow time to query status API over network
        checkoutAndAssertHookOutputContainsStrings(
            "a0578c14c0f4d22de05f23e50b41ad46da4cb16e",
            ["a0578c1", "1 failed build", "FAILED_BUILD"],
            done
        );
    });

    it("1 successful, 1 in progress build", function(done) {
        this.timeout(10000); // allow time to query status API over network
        checkoutAndAssertHookOutputContainsStrings(
            "295326a203d49e1e009d939cdfbb9c6e6081dcfd",
            ["295326a", "1 currently executing build", "INPROGRESS_BUILD"],
            done
        );
    });

    it("11 successful builds", function(done) {
        this.timeout(10000); // allow time to query status API over network
        checkoutAndAssertHookOutputContainsStrings(
            "f84babe7a07c3162c3bdacb496ecae189b82174d",
            ["f84babe", "11 green builds"],
            done
        );
    });

    it("No remotes", function(done) {
        git(["remote", "rm", "origin"], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo
        }).then(function() {
            checkoutHeadExpectingError(["Error", "Remote 'origin' does not exist"], done);
        });
    });

    it("No origin", function (done) {
        git(["remote", "rename", "origin", "bob"], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo
        }).then(function() {
            checkoutHeadExpectingError(["Error", "Remote 'origin' does not exist"], done);
        });
    });

    it("Origin is not SSH or HTTPS", function (done) {
        git(["remote", "set-url", "origin", "bob"], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo
        }).then(function() {
            checkoutHeadExpectingError(["Error", "Couldn't parse domain from origin url 'bob'"], done);
        });
    });

    it("No HEAD", function (done) {
        testRepo = temp.mkdirSync("test-repo");
        git(["init"], {
            stderr: true,
            logger: prettyLog,
            cwd: testRepo
        }).then(function() {
            invokeBitbucketBuildStatusExpectingError(["Error", "Revspec 'HEAD' not found"], done);
        });
    });

    it("Not a Git repo", function (done) {
        testRepo = temp.mkdirSync("test-repo");
        invokeBitbucketBuildStatusExpectingError(["Error", "Could not find repository"], done);
    });

});