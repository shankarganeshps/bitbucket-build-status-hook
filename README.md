# Bitbucket Build Status Hook

This Git `post-checkout` hook displays the latest Bitbucket Cloud 
[build status] for a commit when you check it out in your terminal. This 
allows you to quickly tell if your feature branch is failing, or whether 
it's safe to create a new branch from master!

## Usage

This tool is most useful when symlinked as a `post-commit` hook, which runs every 
time you check out a branch or commit:

![post-checkout hook in action](https://bytebucket.org/snippets/tpettersen/e4kzL/raw/a0f8d75c9de214a73ae09f55c6521844154b0453/aui%20%E2%80%94%20tpettersen%40ninkasi%20%E2%80%94%20-zsh%20%E2%80%94%20189%C3%9753%202016-06-20%2015-28-23.png)

But you can also manually run `bitbucket-build-status` to check the build 
status of any branch, commit, or commit-ish, at any time.

![bitbucket-build-status in action](https://bytebucket.org/snippets/tpettersen/e4kzL/raw/79bb1255eab6de26b71720dda5c839c23a42f9f0/aui%20%E2%80%94%20tpettersen%40ninkasi%20%E2%80%94%20~%3Asrc%3Aatl%3Aaui%20%E2%80%94%20-zsh%20%E2%80%94%20141%C3%9730%202016-06-23%2009-34-22.png)

## Installation

1. Install [Node.js](http://nodejs.org/)
1. Run `npm install -g bitbucket-build-status-hook`
1. Navigate to the root of a Git repository that you've cloned from [Bitbucket]
1. Run `ln -s /usr/local/bin/bitbucket-build-status .git/hooks/post-checkout` 
1. Run `git checkout HEAD` to test that the hook is properly installed

## Credential management

The first time you upload a file you will be prompted for your Bitbucket 
email and password. These will be used to retrieve an OAuth refresh 
token which is stored in `~/.bitbucket-build-status` and used for 
subsequent requests. Treat this token carefully as it can be used to 
read repository data on your behalf.

### 2FA/U2F

If you have 2FA enabled for your Bitbucket account, the client will be 
unable to use OAuth to authenticate you. Instead, you'll need to 
configure a Bitbucket [App Password] for the client to use. To do this: 

1. create an [App Password] with `Read` access to your 
repositories
1. create a file at `~/.bitbucket-build-status-hook` containing:
```
{
    "bitbucket.org": {
        "username": "your-username" // (*not* your email address),
        "password": "your-app-password"
    }
}
```
 
## Bitbucket developers

By default, the API is assumed to live at `api.$domain_of_repo_origin`. 
This is not true for developing against `staging.bb-inf.net` or 
`localhost`. To remap the API, copy `.bitbucket-build-status-hook.sample`
to `~/bitbucket-build-status-hook`. You'll also need to  update the 
`username` and `password` to an [App Password] as described in the 2FA
instructions. 

```
{	
	"staging.bb-inf.net": {
		"username": "kannonboy",
		"password": "password",
		"apiDomain": "api-staging.bb-inf.net"
	}
}
```

For `localhost` or other Bitbucket Cloud hosts, update 
`staging.bb-inf.net` and `apiDomain` as appropriate.  

[build status]: https://blog.bitbucket.org/2015/11/18/introducing-the-build-status-api-for-bitbucket-cloud/
[Bitbucket]: https://bitbucket.org
[App Password]: https://blog.bitbucket.org/2016/06/06/app-passwords-bitbucket-cloud/