#!/usr/bin/env node

// parse arguments
var ref;
switch (process.argv.length) {
    case 3:
        // one argument, assume invoked by user manually with a ref (node bitbucket-build-status $ref)
        ref = process.argv[2];
        break;
    case 5:
        // assume invoked by git as post-checkout hook (node post-checkout $oldSha $newSha $isBranch)
        if (process.argv[4] === 0) {
            // 0 means a file checkout -- do nothing
            process.exit(0);
        }
        ref = process.argv[3];
        break;
    default:
        // assume invoked manually with no args, or ¯\_(ツ)_/¯. Default to HEAD (as per Git's behaviour)
        ref = "HEAD";
        break;
}

module.exports = require("./lib/build-status")({
    ref: ref,
    repoDir: process.cwd()
}).catch(function(err) {
    console.error(err);
});
