var co = require('co');
var prompt = require('co-prompt');
var jsonfile = require('jsonfile');
var path = require('path');
var RSVP = require("rsvp");
var bitbucketAuthToken = require("bitbucket-auth-token");
var config = require("./config");

var consumerInfo = jsonfile.readFileSync(path.join(__dirname, '../consumer.json'));

module.exports = function(commitContext) {
    return new RSVP.Promise(function (resolve, reject) {

        var domainConfig = config[commitContext.domain];

        if (process.env.BB_SKIP_AUTH) {
            resolve(null);
        } else if (domainConfig && domainConfig.username && domainConfig.password) {
            resolve(function(req) {
                req.auth(domainConfig.username, domainConfig.password);
            });
        } else {
            bitbucketAuthToken.getAccessToken({
                appName: 'bitbucket-build-status-' + commitContext.domain,
                domain: commitContext.domain,
                consumerKey: consumerInfo.key,
                consumerSecret: consumerInfo.secret,
                logger: console.log,
                credentialsProvider: function () {
                    return new RSVP.Promise(function (resolve, reject) {
                        co(function *() {
                            resolve({
                                username: (yield prompt('email (' + commitContext.domain + '): ')),
                                password: (yield prompt.password('password: '))
                            });
                        });
                    });
                }
            }).then(function(accessToken) {
                resolve(function(req) {
                    req.set("Authorization", "Bearer " + accessToken);
                });
            });
        }
    });
};