var chalk = require("chalk");
var pluralize = require("pluralize");
var parseCommitContext = require("./commit-context");
var fetchBuilds = require("./fetch-builds");
var logger = require("./logger");

module.exports = function (opts) {
    return parseCommitContext(opts).then(function (commitContext) {
        return fetchBuilds(commitContext).then(function (builds) {
            if (builds.failed.length) {
                logger.log(
                    chalk.red("Oh no! ") +
                    chalk.white(commitContext.shortSha) +
                    chalk.red(" has " + pluralize("failed builds", builds.failed.length, true) + " (" + builds.failed.join(", ") + ")") +
                    "\nDetails at " + chalk.underline(commitContext.commitUrl)
                );
            } else if (builds.inprogress.length) {
                logger.log(
                    chalk.cyan("Be careful, ") +
                    chalk.white(commitContext.shortSha) +
                    chalk.cyan(" has " + pluralize("currently executing builds", builds.inprogress.length, true) + " (" + builds.inprogress.join(", ") + ")") +
                    "\nDetails at " + chalk.underline(commitContext.commitUrl)
                );
            } else if (builds.successful.length) {
                logger.log(
                    chalk.green("Sweet action! ") +
                    chalk.white(commitContext.shortSha) +
                    chalk.green(" has " + pluralize("green builds", builds.successful.length, true))
                );
            } else {
                logger.log(
                    chalk.white(commitContext.shortSha) +
                    chalk.grey(" hasn't been built yet")
                );
            }
            process.exit(0);
        });

    }).catch(function (err) {
        logger.error(chalk.red(err));
        process.exit(1);
    });
};
