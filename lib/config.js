var jsonfile = require('jsonfile');
var homeDir = require('home-dir');
var fs = require("fs");

var configFilePath = homeDir(".bitbucket-build-status-hook");

function generateConfig() {
    var config = {};
    addHomeDirConfig(config);
    return config;
}

function addHomeDirConfig(config) {
    try {
        fs.accessSync(configFilePath);
    } catch (e) {
        // no file (or no access)
        return;
    }
    try {
        Object.assign(config, jsonfile.readFileSync(configFilePath));
    } catch (e) {
        console.error("Error parsing " + configFilePath + ": " + e);
    }
}

module.exports = generateConfig();
