var RSVP = require('rsvp');
var request = require('superagent');
var auth = require("./auth");

module.exports = function(commitContext) {
    return auth(commitContext).then(function (authenticateRequest) {
        var pageUrl = "https://" + commitContext.apiDomain + "/2.0/repositories/" + commitContext.owner + "/" +
            commitContext.repoSlug + "/commit/" + commitContext.sha + "/statuses";

        return fetchPageOfBuilds(authenticateRequest, pageUrl);
    });
};

function fetchPageOfBuilds(authenticateRequest, pageUrl, builds) {
    builds = builds || {
        successful: [],
        failed: [],
        inprogress: []
    };

    return new RSVP.Promise(function (resolve, reject) {
        var req = request.get(pageUrl);
        authenticateRequest && authenticateRequest(req);
        req.set('Accept', 'application/json')
           .end(function (err, res) {
                if (res && res.ok) {
                    res.body.values.forEach(function (build) {
                        var state = build.state.toLowerCase();
                        if (!builds[state]) {
                            console.log("Unexpected build state: " + state);
                        } else {
                            builds[state].push(build.name);
                        }
                    });

                    if (res.body.next) {
                        resolve(fetchPageOfBuilds(authenticateRequest, res.body.next, builds));
                    } else {
                        resolve(builds);
                    }
                } else {
                    var errorMessage;
                    if (res && res.status === 401) {
                        if (authenticateRequest) {
                            errorMessage = "Authentication failed!";
                        } else {
                            errorMessage = "No authentication was sent, but resource requires authentication.";
                        }
                    } else if (err) {
                        errorMessage = err;
                    } else {
                        errorMessage = res.text;
                    }
                    reject(errorMessage);
                }
            });
    });
}