var fs = require("fs");

if (process.env.BB_HOOK_OUT) {
    // for tests
    function toFile(msg) {
        fs.writeFileSync(process.env.BB_HOOK_OUT, msg);
    }
    module.exports = {
        log: toFile,
        error: toFile
    };
} else {
    module.exports = console;
}
