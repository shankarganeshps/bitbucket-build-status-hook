var RSVP = require('rsvp');
var Git = require('nodegit');
var config = require('./config');

module.exports = function(opts) {
    return new RSVP.Promise(function (resolve, reject) {
        Git.Repository.open(opts.repoDir)
            .then(function (repo) {
                resolve({
                    repo: repo
                });
            }).catch(function(err) {
                reject(err);
            });
    }).then(function (commitContext) {
        return new RSVP.Promise(function (resolve, reject) {
            Git.AnnotatedCommit.fromRevspec(commitContext.repo, opts.ref, function (err, commit) {
                if (err) {
                    reject(err);
                } else {
                    commitContext.sha = commit.id().tostrS();
                    commitContext.shortSha = commitContext.sha.substring(0, 7);
                    resolve(commitContext);
                }
            });
        });
    }).then(function (commitContext) {
        return new RSVP.Promise(function (resolve, reject) {
            commitContext.repo.getRemote("origin", function (err, remote) {
                if (err) {
                    reject(err);
                } else {
                    commitContext.origin = remote;
                    resolve(commitContext);
                }
            });
        });
    }).then(function (commitContext) {
        return new RSVP.Promise(function (resolve, reject) {
            var result = /(?:@|\/\/)(.*?)[/:]([^/:]+)\/([^/]+?)(?:\.git)?$/.exec(commitContext.origin.url());

            if (!result) {
                reject("Error: Couldn't parse domain from origin url '" + commitContext.origin.url() + "'");
                return;
            }

            commitContext.domain = result[1];
            commitContext.apiDomain = "api." + commitContext.domain;

            var domainConfig = config[commitContext.domain];
            if (domainConfig) {
                commitContext.domain = domainConfig.domain || commitContext.domain;
                commitContext.apiDomain = domainConfig.apiDomain || commitContext.apiDomain;
            }
            
            commitContext.owner = result[2];
            commitContext.repoSlug = result[3];
            commitContext.commitUrl = "https://" + commitContext.domain + "/" + commitContext.owner + 
                                      "/" + commitContext.repoSlug + "/commits/" + commitContext.sha;
            resolve(commitContext);
        });
    });
};
        
        